import { Express, Request, Response } from "express";
import { AppComponents } from "./app/interfaces";
import { statusLogic } from "./logic/status";
import { friendshipsLogic } from "./logic/friendships";
import { usersPositionLogic } from "./logic/usersUpdate";
import { asyncHandler } from "./utils/express-utils";

export async function configureRoutes(
  expressApp: Express,
  components: AppComponents
) {
  const friendships = friendshipsLogic(components);
  const usersUpdate = usersPositionLogic(components);
  const status = statusLogic();

  expressApp.get("/status", (_req, res) => res.send(status.getStatus()));

  expressApp.get(
    "/friendships",
    asyncHandler(async (_req, res) => {
      res.send(await friendships.getAll());
    }, components)
  );

  // New Endpoint to update the users positions
  
  expressApp.get(
    "/users-update",
    asyncHandler(async (_, res: Response) => {
      res.send(await usersUpdate.getAll());
    }, components)
  );
  expressApp.get(
    "/suggest-friendship/:user1/:user2",
    asyncHandler(async (req: Request, res: Response) => {
      res.send(await friendships.getFriendshipById({
        user1: req.params.user1,
        user2: req.params.user2
      }));
    }, components)
  );

  expressApp.post(
    "/users-update",
    asyncHandler(async (req: Request, res: Response) => {
      let { body } = req;
      res.send(await usersUpdate.create(body.moved, body.disconnected));
    }, components)
  );
  

  expressApp.post(
    "/friendships/:address1/:address2",
    asyncHandler(async (req, res) => {
      res.send(
        await friendships.create({
          userAddress1: req.params.address1,
          userAddress2: req.params.address2,
        })
      );
    }, components)
  );
}
