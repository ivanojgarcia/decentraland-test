import pgPromise, { IDatabase } from "pg-promise";
import { DEFAULT_DATABASE_CONFIG } from "../config/db";
import { FriendshipsRepository, UserUpdateRepository } from "../entities/types";
import { createFriendshipsRepo as createFriendshipsRepo } from "./DbFriendshipsRepo";
import { createUserPositionRepo as createUserPositionRepo } from "./DbUserPositionRepo";

interface DbExtensions {
  users: FriendshipsRepository;
  usersPosition: UserUpdateRepository;
}

export type Database = IDatabase<DbExtensions> & DbExtensions;

export function createDatabase(): Database {
  const pgp = pgPromise({
    extend: (db: Database) => {
      db.users = createFriendshipsRepo(db);
      db.usersPosition = createUserPositionRepo(db);
    },
  });

  return pgp(DEFAULT_DATABASE_CONFIG);
}
