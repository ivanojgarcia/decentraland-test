import {UserUpdateRepository, UserPosition, User } from "../entities/types";
import { Database } from "./database";

export function createUserPositionRepo(db: Database): UserUpdateRepository {
  return {
    async getAll() {
      return await db.manyOrNone("SELECT * FROM USER_POSITION");
    },
    async create(userPosition: UserPosition) {
      await db.none(
        `INSERT INTO public.user_position
        (id, "position", disconnected)
        VALUES($1, $2, $3);`,
        [userPosition.id, userPosition.position, userPosition.disconnected]
      );
      return userPosition;
    },

    async exists(id: string) {
      return (
        await db.one(
          'SELECT EXISTS (SELECT 1 FROM user_position WHERE ("id" = $1))',
          [id]
        )
      ).exists as boolean;
    },

    async update(userPosition: UserPosition) {
      await db.none(
        `UPDATE user_position
        SET "position"=$1, disconnected=$2
        WHERE id=$3;`,
        [ userPosition.position, userPosition.disconnected, userPosition.id ]
      );
      return userPosition;
    },
    async disconnectUser(id: string) {
      await db.none(
        `UPDATE user_position
        SET disconnected=$1
        WHERE id=$2;`,
        [ true, id ]
      );
      return id;
    }
  };
}
