import { AppComponents } from "../app/interfaces";
import { UsersPositionInput, UserPosition, UserUpdateRepository, User, ShouldSuggest } from "../entities/types";
import { ServiceError } from "../utils/express-utils";

const disconnecUsers = async (users: string[], userUpdateRepo: UserUpdateRepository): Promise<string[]> => {
  try {
    let usersDisconnected: string[] = [];
    for (let user of users) {
      if(await userUpdateRepo.exists(user)) {
        let userDisc: string = await userUpdateRepo.disconnectUser(user);
        usersDisconnected.push(userDisc);
      }
    }
    
    return usersDisconnected;
  } catch (error) {
    let message: string;
    if (error instanceof Error) message = error.message
    else message = String(error)
    throw new ServiceError(message, 500);
  }
}

export function usersPositionLogic({
  userUpdateRepo,
}: Pick<AppComponents, "userUpdateRepo">) {
  return {
    async getAll() {
      try {
        return userUpdateRepo.getAll();
      } catch (error) {
        let message: string;
        if (error instanceof Error) message = error.message
        else message = String(error)
        throw new ServiceError(message, 500);
      }
    },

    async create(users: UsersPositionInput[], disconnected: string[]) {
      try {
        let usersUpdated: UserPosition[] = [];
        let usersDisconnected: string[] = [];
     
        for (let user of users) {
          let userInput = {
            id: user.id,
            position: JSON.stringify(user.position),
            disconnected: false
          }
          const existUser = await userUpdateRepo.exists(user.id);
          if(!existUser){
            let userdata = await userUpdateRepo.create(userInput);
            usersUpdated.push(userdata)
          } else {
            let userdata = await userUpdateRepo.update(userInput);
            usersUpdated.push(userdata)
          }

          if(disconnected.length > 0) {
            usersDisconnected = await disconnecUsers(disconnected, userUpdateRepo);
          }
        }
        return {
          moved: usersUpdated,
          disconnected: usersDisconnected
        };
      } catch (error) {
        let message: string;
        if (error instanceof Error) message = error.message
        else message = String(error)
        throw new ServiceError(message, 500);
      }
    },
  };
}