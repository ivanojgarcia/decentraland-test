import { AppComponents } from "../app/interfaces";
import { Friendship, User, ShouldSuggest } from "../entities/types";
import { ServiceError } from "../utils/express-utils";

export function friendshipsLogic({
  friendshipsRepo,
}: Pick<AppComponents, "friendshipsRepo">) {
  return {
    async getAll() {
      return friendshipsRepo.getAll();
    },
    async getFriendshipById(user: User) {
      try {
        const friendshipConnection = await friendshipsRepo.getFriendshipById(user);
        return friendshipConnection.length > 0 ? {
          shouldSuggest: true
        } : 
        {
          shouldSuggest: false
        }
      } catch (error) {
        let message: string;
        if (error instanceof Error) message = error.message
        else message = String(error)
        throw new ServiceError(message, 500);
      }
    },
    async create(friendship: Friendship) {
      if (await friendshipsRepo.exists(friendship))
        throw new ServiceError("The friendship already exists");
      return friendshipsRepo.create(friendship);
    },
  };
}
