
export interface FriendshipsRepository {
  create(friendship: Friendship): Promise<Friendship>;
  exists(friendship: Friendship): Promise<boolean>;
  getAll(): Promise<Friendship[]>;
  getFriendshipById(user: User): Promise<Friendship[]>;
}

export interface UserUpdateRepository {
  create(userPosition: UserPosition): Promise<UserPosition>;
  update(userPosition: UserPosition): Promise<UserPosition>;
  disconnectUser(id: String): Promise<string>;
  getAll(): Promise<UserPosition[]>;
  exists(id: string):  Promise<boolean>;
}

export type User = {
  user1: string;
  user2: string;
}

export type ShouldSuggest = {
  shouldSuggest: boolean
}

export type Friendship = {
  userAddress1: string;
  userAddress2: string;
};

export type UserPosition = {
  id: string;
  position: Position | string;
  disconnected?: boolean;
  createdAt?: any
}

export type UserPositionReturn = {
  moved: UserPosition[],
  disconnected: string[]
}

export type Position = {
  x: number;
  y: number
}

export interface UsersPositionInput {
  id: string;
  position: Position
}
