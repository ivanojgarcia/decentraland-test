export const DEFAULT_DATABASE_CONFIG = {
  password: "postgres",
  user: "postgres",
  database: "users_tracker",
  host: "localhost",
  port: 5432,
};
