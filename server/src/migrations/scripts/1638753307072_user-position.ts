/* eslint-disable @typescript-eslint/camelcase */
import { MigrationBuilder, ColumnDefinitions } from 'node-pg-migrate';

export const shorthands: ColumnDefinitions | undefined = undefined;

export async function up(pgm: MigrationBuilder): Promise<void> {
    pgm.createTable("user_position", {
        id: { type: 'text', notNull: true, primaryKey: true },
        position: { type: 'json', notNull: true },
        disconnected: {type: 'boolean', notNull: false},
        createdAt: {
            type: 'timestamp',
            notNull: true,
            default: pgm.func('current_timestamp'),
        },
    })
}

export async function down(pgm: MigrationBuilder): Promise<void> {
    pgm.dropTable("user_position");
}
